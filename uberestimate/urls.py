from django.conf.urls import url, include
from django.views.generic import TemplateView
from views import EstimateView

urlpatterns = [
    url(r'^estimate/', EstimateView.as_view(), name='estimate'),
    url(r'^$', TemplateView.as_view(template_name='uberestimate/index.html'), name='index')
]