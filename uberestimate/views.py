from django.http import HttpResponse
from django.views.generic import View
import json
import urllib2
from getuberestimate.settings import UBER_SERVER_TOKEN

# Create your views here.


class EstimateView(View):

    def post(self, *args, **kwargs):
        start_latitude = self.request.POST['start_latitude']
        start_longitude = self.request.POST['start_longitude']
        end_latitude = self.request.POST['end_latitude']
        end_longitude = self.request.POST['end_longitude']

        uber_url = 'https://api.uber.com/v1/estimates/price?start_latitude=%s&start_longitude=%s&end_latitude=%s&end_longitude=%s&server_token=%s' % \
                (start_latitude, start_longitude, end_latitude, end_longitude, UBER_SERVER_TOKEN)

        resp = urllib2.urlopen(uber_url).read()
        return HttpResponse(resp, content_type='application/json')

